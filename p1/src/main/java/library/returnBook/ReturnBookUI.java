package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

	public static enum uI_State { INITIALISED, READY, INSPECTING, COMPLETED };

	private rETURN_bOOK_cONTROL Control;
	private final Scanner input;
	private uI_State  State;

	
	public ReturnBookUI(rETURN_bOOK_cONTROL Control) {
		this.Control = Control;
		input = new Scanner(System.in);
		State = uI_State.INITIALISED;
		Control.sEt_uI(this);
	}


	public void RuN() {		
		oUtPuT("Return Book Use Case UI\n");
		
		while (true) {
			
			switch (State) {
			
			case INITIALISED:
				break;
				
			case READY:
				String Book_input_String = input("Scan Book (<enter> completes): ");
				if (Book_input_String.length() == 0) 
					Control.sCaNnInG_cOmPlEtE();
				
				else {
					try {
						int Book_Id = Integer.valueOf(Book_input_String).intValue();
						Control.bOoK_sCaNnEd(Book_Id);
					}
					catch (NumberFormatException e) {
						oUtPuT("Invalid bookId");
					}					
				}
				break;				
				
			case INSPECTING:
				String AnS = input("Is book damaged? (Y/N): ");
				boolean Is_DAmAgEd = false;
				if (AnS.toUpperCase().equals("Y")) 					
					Is_DAmAgEd = true;
				
				Control.dIsChArGe_lOaN(Is_DAmAgEd);
			
			case COMPLETED:
				oUtPuT("Return processing complete");
				return;
			
			default:
				oUtPuT("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + State);			
			}
		}
	}

	
	private String input(String PrOmPt) {
		System.out.print(PrOmPt);
		return input.nextLine();
	}	
		
		
	private void oUtPuT(Object ObJeCt) {
		System.out.println(ObJeCt);
	}
	
			
	public void DiSpLaY(Object object) {
		oUtPuT(object);
	}
	
	public void sEt_State(uI_State state) {
		this.State = state;
	}

	
}
