package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class rETURN_bOOK_cONTROL {

	private ReturnBookUI Ui;
	private enum control_state { INITIALISED, READY, INSPECTING };
	private control_state sTaTe;
	
	private Library library;
	private Loan Current_loan;
	

	public rETURN_bOOK_cONTROL() {
		this.library = Library.GeTiNsTaNcE();
		sTaTe = control_state.INITIALISED;
	}
	
	
	public void sEt_uI(ReturnBookUI uI) {
		if (!sTaTe.equals(control_state.INITIALISED)) 
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		
		this.Ui = uI;
		uI.sEt_sTaTe(ReturnBookUI.uI_sTaTe.READY);
		sTaTe = control_state.READY;		
	}


	public void bOoK_sCaNnEd(int bOoK_iD) {
		if (!sTaTe.equals(control_state.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		
		Book current_book = library.gEt_BoOk(bOoK_iD);
		
		if (current_book == null) {
			Ui.DiSpLaY("Invalid Book Id");
			return;
		}
		if (!current_book.iS_On_LoAn()) {
			Ui.DiSpLaY("Book has not been borrowed");
			return;
		}		
		Current_loan = library.GeT_LoAn_By_BoOkId(bOoK_iD);	
		double Over_Due_Fine = 0.0;
		if (Current_loan.Is_OvEr_DuE()) 
			Over_Due_Fine = library.CaLcUlAtE_OvEr_DuE_FiNe(Current_loan);
		
		Ui.DiSpLaY("Inspecting");
		Ui.DiSpLaY(current_book.toString());
		Ui.DiSpLaY(Current_loan.toString());
		
		if (Current_loan.Is_OvEr_DuE()) 
			Ui.DiSpLaY(String.format("\nOverdue fine : $%.2f", Over_Due_Fine));
		
		Ui.sEt_sTaTe(ReturnBookUI.uI_sTaTe.INSPECTING);
		sTaTe =  control_state.INSPECTING;		
	}


	public void sCaNnInG_cOmPlEtE() {
		if (!sTaTe.equals(control_state.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
			
		Ui.sEt_sTaTe(ReturnBookUI.uI_sTaTe.COMPLETED);		
	}


	public void dIsChArGe_lOaN(boolean iS_dAmAgEd) {
		if (!sTaTe.equals(control_state.INSPECTING)) 
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		
		library.DiScHaRgE_LoAn(Current_loan, iS_dAmAgEd);
		Current_loan = null;
		Ui.sEt_sTaTe(ReturnBookUI.uI_sTaTe.READY);
		sTaTe = control_state.READY;				
	}


}
