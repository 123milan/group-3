package library.Fixbook;
import library.entities.Book;
import library.entities.Library;

public class Fix_bOOK_Control {
	
	private FixBookUI Ui;
	private enum Control_StAtE { INITIALISED, READY, FIXING };
	private Control_StAtE StAtE;
	
	private final Library LiBrArY;
	private Book CuRrEnT_BoOk;


	public Fix_bOOK_Control() {
		this.LiBrArY = Library.GeTINSTANCE();
		StAtE = Control_StAtE.INITIALISED;
	}
	
	
	public void SeT_Ui(FixBookUI ui) {
		if (!StAtE.equals(Control_StAtE.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = ui;
		ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
		StAtE = Control_StAtE.READY;		
	}


	public void BoOk_ScAnNeD(int BoOkId) {
		if (!StAtE.equals(Control_StAtE.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		CuRrEnT_BoOk = LiBrArY.gEt_BoOk(BoOkId);
		
		if (CuRrEnT_BoOk == null) {
			Ui.dIsPlAy("Invalid bookId");
			return;
		}
		if (!CuRrEnT_BoOk.iS_DaMaGeD()) {
			Ui.dIsPlAy("Book has not been damaged");
			return;
		}
		Ui.dIsPlAy(CuRrEnT_BoOk.toString());
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.FIXING);
		StAtE = Control_StAtE.FIXING;		
	}


	public void Fix_BoOk(boolean mUsT_Fix) {
		if (!StAtE.equals(Control_StAtE.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call FixBook except in FIXING state");
			
		if (mUsT_Fix) 
			LiBrArY.RePaIr_BoOk(CuRrEnT_BoOk);
		
		CuRrEnT_BoOk = null;
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
		StAtE = Control_StAtE.READY;		
	}

	
	public void SCannING_COMplete() {
		if (!StAtE.equals(Control_StAtE.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		Ui.SeT_StAtE(FixBookUI.uI_sTaTe.COMPLETED);		
	}

}
