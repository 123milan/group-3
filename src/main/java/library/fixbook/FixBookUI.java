package library.Fixbook;
import java.util.Scanner;


public class FixBookUI {

	public static enum uI_sTaTe { INITIALISED, READY, FIXING, COMPLETED };

	private Fix_bOOK_Control Control;
	private Scanner Input;
	private uI_sTaTe StAtE;

	
	public FixBookUI(Fix_bOOK_Control Control) {
		this.Control = Control;
		Input = new Scanner(System.in);
		StAtE = uI_sTaTe.INITIALISED;
		Control.SeT_Ui(this);
	}


	public void SeT_StAtE(uI_sTaTe state) {
		this.StAtE = state;
	}

	
	public void RuN() {
		Output("Fix Book Use Case UI\n");
		
		while (true) {
			
			switch (StAtE) {
			
			case READY:
				String BoOk_EnTrY_StRiNg = Input("Scan Book (<enter> completes): ");
				if (BoOk_EnTrY_StRiNg.length() == 0) 
					Control.SCannING_COMplete();
				
				else {
					try {
						int BoOk_Id = Integer.valueOf(BoOk_EnTrY_StRiNg).intValue();
						Control.BoOk_ScAnNeD(BoOk_Id);
					}
					catch (NumberFormatException e) {
						Output("Invalid bookId");
					}
				}
				break;	
				
			case FIXING:
				String AnS = Input("Fix Book? (Y/N) : ");
				boolean Fix = false;
				if (AnS.toUpperCase().equals("Y")) 
					Fix = true;
				
				Control.Fix_BoOk(Fix);
				break;
								
			case COMPLETED:
				Output("Fixing process complete");
				return;
			
			default:
				Output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + StAtE);			
			
			}		
		}
		
	}

	
	private String Input(String prompt) {
		System.out.print(prompt);	
		return Input.nextLine();
	}	
		
			
	private void Output(Object object) {
		System.out.println(object);
	}
		

	public void dIsPlAy(Object object) {
		Output(object);
	}
	
	
}
