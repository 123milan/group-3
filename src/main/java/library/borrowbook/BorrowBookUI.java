package library.borrowbook;
import java.awt.SystemColor;
import java.util.Scanner;


public class BorrowBookUI {

    private final SystemColor Control;
	
	public static enum uI_STaTe { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private final bORROW_bOOK_Control Control;
	private final Scanner InPuT;
	private uI_STaTe StaTe;

    /**
     *
     * @param Control
     */
    public BorrowBookUI(bORROW_bOOK_Control Control) {
		this.Control = Control;
		InPuT = new Scanner(System.in);
		StaTe = uI_STaTe.INITIALISED;
		Control.SeT_Ui(this);
            this.Control = Control;
	}

	
	private String iNpUT(String Prompt) {
		System.out.print(Prompt);
		return InPuT.nextLine();
	}	
		
		
	private void OuTpUt(Object ObJeCt) {
		System.out.println(ObJeCt);
	}
	
			
	public void SeT_StAtE(uI_STaTe StAtE) {
		this.StaTe = StAtE;
	}

	
	public void RuN() {
		OuTpUt("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (StaTe) {			
			
			case CANCELLED -> {
                            OuTpUt("Borrowing Cancelled");
                            return;
                        }

				
			case READY -> {
                            String MEM_STR = iNpUT("Swipe member card (press <enter> to cancel): ");
                            if (MEM_STR.length() == 0) {
                                Control.CaNcEl();
                                break;
                            }
                            try {
                                int MeMbEr_Id = Integer.parseInt(MEM_STR);
                                Control.SwIpEd(MeMbEr_Id);
                            }
                            catch (NumberFormatException e) {
                                OuTpUt("Invalid Member Id");
                            }
                        }

				
			case RESTRICTED -> {
                            iNpUT("Press <any key> to cancel");
                            Control.CaNcEl();
                        }
			
				
			case SCANNING -> {
                            String BoOk_StRiNg_InPuT = iNpUT("Scan Book (<enter> completes): ");
                            if (BoOk_StRiNg_InPuT.length() == 0) {
                                Control.CoMpLeTe();
                                break;
                            }
                            try {
                                int BiD = Integer.valueOf(BoOk_StRiNg_InPuT).intValue();
                                Control.ScAnNeD(BiD);
                                
                            } catch (NumberFormatException e) {
                                OuTpUt("Invalid Book Id");
                            }
                        }
					
				
			case FINALISING -> {
                            String AnS = iNpUT("Commit loans? (Y/N): ");
                            if (AnS.toUpperCase().equals("N")) {
                                Control.CaNcEl();
                                
                            } else {
                                Control.CoMmIt_LoAnS();
                                iNpUT("Press <any key> to complete ");
                            }
                        }
				
				
			case COMPLETED -> {
                            OuTpUt("Borrowing Completed");
                            return;
                        }
	
				
			default -> {
                            OuTpUt("Unhandled state");
                            throw new RuntimeException("BorrowBookUI : unhandled state :" + StaTe);			
                        }
			}
		}		
	}


	public void DiSpLaY(Object object) {
		OuTpUt(object);		
	}


}
