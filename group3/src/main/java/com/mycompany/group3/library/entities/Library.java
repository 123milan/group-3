package com.mycompany.group3.library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String libraryFiLe = "library.obj";
	private static final int loanLimit = 2;
	private static final int loanPeriod = 2;
	private static final double Fine_Per_Day = 1.0;
	private static final double maxFinesOwed = 1.0;
	private static final double damageFee = 2.0;
	
	private static Library SeLf;
	private int book_Id;
	private int member_Id;
	private int loan_Id;
	private Date loan_Date;
	
	private final Map<Integer, Book> Catalog;
	private final Map<Integer, Member> Members;
	private final Map<Integer, Loan> Loans;
	private final Map<Integer, Loan> Current_LoanS;
	private final Map<Integer, Book> Damaged_Books;
	

	private Library() {
		Catalog = new HashMap<>();
		Members = new HashMap<>();
		Loans = new HashMap<>();
		Current_LoanS = new HashMap<>();
		Damaged_Books = new HashMap<>();
		book_Id = 1;
		member_Id = 1;		
		loan_Id = 1;		
	}

	
	public static synchronized Library GeTiNsTaNcE() {		
		if (SeLf == null) {
			Path PATH = Paths.get(libraryFiLe);			
			if (Files.exists(PATH)) {	
				try (ObjectInputStream LiBrArY_FiLe = new ObjectInputStream(new FileInputStream(libraryFiLe));) {
			    
					SeLf = (Library) LiBrArY_FiLe.readObject();
					Calendar.gEtInStAnCe().SeT_DaTe(SeLf.loan_Date);
					LiBrArY_FiLe.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else SeLf = new Library();
		}
		return SeLf;
	}

	
	public static synchronized void SaVe() {
		if (SeLf != null) {
			SeLf.loan_Date = Calendar.gEtInStAnCe().gEt_DaTe();
			try (ObjectOutputStream LiBrArY_fIlE = new ObjectOutputStream(new FileOutputStream(libraryFiLe));) {
				LiBrArY_fIlE.writeObject(SeLf);
				LiBrArY_fIlE.flush();
				LiBrArY_fIlE.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int gEt_book_Id() {
		return book_Id;
	}
	
	
	public int gEt_member_Id() {
		return member_Id;
	}
	
	
	private int gEt_NeXt_book_Id() {
		return book_Id++;
	}

	
	private int gEt_NeXt_member_Id() {
		return member_Id++;
	}

	
	private int gEt_NeXt_loan_Id() {
		return loan_Id++;
	}

	
	public List<Member> lIsT_Members() {		
		return new ArrayList<>(Members.values()); 
	}


	public List<Book> LIST_BOOKS() {		
		return new ArrayList<>(Catalog.values()); 
	}


	public List<Loan> lISt_Current_LoanS() {
		return new ArrayList<>(Current_LoanS.values());
	}


	public Member aDd_MeMbEr(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, gEt_NeXt_member_Id());
		Members.put(member.GeT_ID(), member);		
		return member;
	}

	
	public Book ADD_BOOK(String a, String t, String c) {		
		Book b = new Book(a, t, c, gEt_NeXt_book_Id());
		Catalog.put(b.gEtId(), b);		
		return b;
	}

	
	public Member gEt_MeMbEr(int memberId) {
		if (Members.containsKey(memberId)) 
			return Members.get(memberId);
		return null;
	}

	
	public Book gEt_BoOk(int bookId) {
		if (Catalog.containsKey(bookId)) 
			return Catalog.get(bookId);		
		return null;
	}

	
	public int gEt_LoAn_LiMiT() {
		return loanLimit;
	}

	
	public boolean cAn_MeMbEr_BoRrOw(Member member) {		
		if (member.gEt_nUmBeR_Of_Current_LoanS() == loanLimit ) 
			return false;
				
		if (member.FiNeS_OwEd() >= maxFinesOwed) 
			return false;
				
		for (Loan loan : member.GeT_Loans()) 
			if (loan.Is_OvEr_DuE()) 
				return false;
			
		return true;
	}

	
	public int gEt_NuMbEr_Of_Loans_ReMaInInG_FoR_MeMbEr(Member MeMbEr) {		
		return loanLimit - MeMbEr.gEt_nUmBeR_Of_Current_LoanS();
	}

	
	public Loan iSsUe_LoAn(Book book, Member member) {
		Date dueDate = Calendar.gEtInStAnCe().gEt_DuE_DaTe(loanPeriod);
		Loan loan = new Loan(gEt_NeXt_loan_Id(), book, member, dueDate);
		member.TaKe_OuT_LoAn(loan);
		book.BoRrOw();
		Loans.put(loan.GeT_Id(), loan);
		Current_LoanS.put(book.gEtId(), loan);
		return loan;
	}
	
	
	public Loan GeT_LoAn_By_BoOkId(int bookId) {
		if (Current_LoanS.containsKey(bookId)) 
			return Current_LoanS.get(bookId);
		
		return null;
	}

	
	public double CaLcUlAtE_OvEr_DuE_FiNe(Loan LoAn) {
		if (LoAn.Is_OvEr_DuE()) {
			long DaYs_OvEr_DuE = Calendar.gEtInStAnCe().GeT_DaYs_DiFfErEnCe(LoAn.GeT_DuE_DaTe());
			double fInE = DaYs_OvEr_DuE * Fine_Per_Day;
			return fInE;
		}
		return 0.0;		
	}


	public void DiScHaRgE_LoAn(Loan cUrReNt_LoAn, boolean iS_dAmAgEd) {
		Member mEmBeR = cUrReNt_LoAn.GeT_MeMbEr();
		Book bOoK  = cUrReNt_LoAn.GeT_BoOk();
		
		double oVeR_DuE_FiNe = CaLcUlAtE_OvEr_DuE_FiNe(cUrReNt_LoAn);
		mEmBeR.AdD_FiNe(oVeR_DuE_FiNe);	
		
		mEmBeR.dIsChArGeLoAn(cUrReNt_LoAn);
		bOoK.ReTuRn(iS_dAmAgEd);
		if (iS_dAmAgEd) {
			mEmBeR.AdD_FiNe(damageFee);
			Damaged_Books.put(bOoK.gEtId(), bOoK);
		}
		cUrReNt_LoAn.DiScHaRgE();
		Current_LoanS.remove(bOoK.gEtId());
	}


	public void cHeCk_Current_LoanS() {
		for (Loan lOaN : Current_LoanS.values()) 
			lOaN.cHeCk_OvEr_DuE();
				
	}


	public void RePaIr_BoOk(Book cUrReNt_BoOk) {
		if (Damaged_Books.containsKey(cUrReNt_BoOk.gEtId())) {
			cUrReNt_BoOk.RePaIr();
			Damaged_Books.remove(cUrReNt_BoOk.gEtId());
		}
		else 
			throw new RuntimeException("Library: repairBook: book is not damaged");
		
		
	}
	
	
}
